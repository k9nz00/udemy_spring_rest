package com.semka.spring.rest.controller;

import com.semka.spring.rest.entity.Employee;
import com.semka.spring.rest.exception_handing.NoSuchEmployeeException;
import com.semka.spring.rest.service.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api")
public class MyREStController {

    @Autowired
    private EmployeeService employeeService;

    @GetMapping("/employees")
    public List<Employee> showAllEmployees() {

        List<Employee> allEmployees = employeeService.getAllEmployees();
        return allEmployees;
    }

    @GetMapping("/employees/{id}")
    public Employee getEmployee(@PathVariable int id) {
        Employee employee = employeeService.getEmployee(id);

        if (employee == null) {
            throw new NoSuchEmployeeException(String.format("Работник с id = %s  не найден в базе данных", id));
        }
        return employee;
    }

    @PostMapping("/employees")
    public Employee getEmployee(@RequestBody Employee employee) {
        employeeService.saveEmployee(employee);
        return employee;
    }

    @PutMapping("/employees")
    public Employee updateEmployee(@RequestBody Employee employee) {
        employeeService.saveEmployee(employee);
        return employee;
    }

    @DeleteMapping("/employees/{id}")
    public String deleteEmployee(@PathVariable int id){

        Employee employee = employeeService.getEmployee(id);
        if (employee != null){
            employeeService.deleteEmployee(id);
            return String.format("The employee with id = %d has been deleted", id);
        } else {
            throw new NoSuchEmployeeException(String.format("Работник с id = %s  не найден в базе данных", id));
        }
    }
}
