package com.semka.spring.rest.exception_handing;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class EmployeeIncorrectData {
    private String info;
}
